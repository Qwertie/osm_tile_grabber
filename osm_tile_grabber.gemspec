# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "osm_tile_grabber/version"

Gem::Specification.new do |spec|
  spec.name          = "osm_tile_grabber"
  spec.version       = OsmTileGrabber::VERSION
  spec.authors       = ["Luke Picciau"]
  spec.email         = ["luke.b.picciau@gmail.com"]

  spec.summary       = "Gem for creating an image from OSM of an area"
  spec.description   = "Gem for creating an image from OSM of an area"
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"

  spec.add_dependency "rmagick" 
end
