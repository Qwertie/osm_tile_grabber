# OSM Tile Grabber
[![Gem Version](https://badge.fury.io/rb/osm_tile_grabber.svg)](https://badge.fury.io/rb/osm_tile_grabber)

A simple gem for getting an image of a desired location using OSM tiles. This is fairly beta
software and will likely change soon to make it nicer to use.

# Usage

**Note:** You need to have rmagick installed for this gem to work. I will work on making this easier but for now just follow the instructions on the readme https://github.com/rmagick/rmagick


## Usage warning:
Downloading large amounts of data from the openstreetmap.org tileserver is strictly forbidden. This gem is **NOT** to be used for downloading large maps from OSM for offline use. 
If you need local copies of large amounts of OSM tiles you must set up your own tile server or find a new tileserver where this is permitted.
More info can be found at: https://operations.osmfoundation.org/policies/tiles/

---

Create an instance of OSMTileGrabber

`OSMTileGrabber.new(lat1, lon1, lat2, lon2, zoom)`

lat1 and lon1 are the coordinates of the top left corner of the map you wish to download. lat2 and
lon2 is the bottom right coordinate. Zoom is the zoom level you wish to download the map at (1-19)
a higher zoom will create a bigger image with more detail.

to download the image run `create_image("<output_path>")` on the object instance. This will save the
final image at the path provided.

## Example 

```
require 'osm_tile_grabber'
osm = OSMTileGrabber.new(-34.9220585,138.5879188,-34.9397,138.6239,15)
osm.create_image("/tmp/output.png")
```

![example output map](https://i.imgur.com/Fjmglog.png)