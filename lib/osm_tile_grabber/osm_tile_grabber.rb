require 'open-uri'
require 'tempfile'
require 'rmagick'

class OSMTileGrabber
	include Magick

	def initialize(lat1, lon1, lat2, lon2, zoom)
		@lat1 = lat1
		@lon1 = lon1
		@lat2 = lat2
		@lon2 = lon2
		@zoom = zoom
	end

	def create_image(output_path)
		self.montage(self.download_tiles, output_path)
	end
	
	def tile_numbers_needed
		top_left_tile = get_tile_number(@lat1, @lon1)
		bottom_right_tile = get_tile_number(@lat2, @lon2)
		tile_matrix = []

		(top_left_tile[:y]..bottom_right_tile[:y]).each do |y|
			row = []
			(top_left_tile[:x]..bottom_right_tile[:x]).each do |x|
				row << {x: x, y: y}
			end
			tile_matrix << row
		end
		return tile_matrix
	end

	def image_dimensions
		top_left_tile = get_tile_number(@lat1, @lon1)
		bottom_right_tile = get_tile_number(@lat2, @lon2)
		
		x = bottom_right_tile[:x] - top_left_tile[:x]
		y = bottom_right_tile[:y] - top_left_tile[:y]
		return {x: x + 1, y: y + 1}
	end

	def get_tile_number(lat_as_deg, lon_as_deg)
		lat_as_rad = lat_as_deg/180 * Math::PI
		n = 2.0 ** @zoom
		x = ((lon_as_deg + 180.0) / 360.0 * n).to_i
		y = ((1.0 - Math::log(Math::tan(lat_as_rad) + (1 / Math::cos(lat_as_rad))) / Math::PI) / 2.0 * n).to_i
  
		return {:x => x, :y =>y}
	end

	def link_from_tile_number(x,y)
		# TODO: Allow picking own tileserver
		"https://a.tile.openstreetmap.org/#{@zoom}/#{x}/#{y}.png"	
	end

	def download_tiles
		tile_files = []
		tile_numbers_needed.each do |row|
			row.each do |tn|
				tile_file = Tempfile.new(["tile-#{@zoom}-#{tn[:x]}-#{tn[:y]}", ".png"])
				open(link_from_tile_number(tn[:x], tn[:y]), "rb") do |read_file|
					tile_file.write(read_file.read)
				end
				tile_files << tile_file
			end
		end
		return tile_files
	end

	def montage(tiles, output_path)
		image_list = ImageList.new *tiles.map{|t| t.path}
		tile_string = "#{image_dimensions[:x]}x#{image_dimensions[:y]}"
		tiled_montage = image_list.montage do
			self.geometry = "+0+0"
			self.tile = tile_string
		end
		tiled_montage.write(output_path)
		return output_path
	end
end
